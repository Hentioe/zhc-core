package io.bluerain.zhclean.demo;

import io.reactivex.Observable;
import retrofit2.http.GET;

import java.util.List;

public interface ITweetApi {

    @GET("/v1/tweets")
    Observable<List<TweetModel>> getTweetList();
}
