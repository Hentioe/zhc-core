package io.bluerain.zhclean.demo;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import java.util.List;

public class TweetService {

    private ITweetApi api;

    public TweetService() {
        this.setApi(RetrofitBuilder.instance().create(ITweetApi.class));
    }

    public Observable<List<TweetModel>> getAll() {
        return getApi().getTweetList();
    }

    private ITweetApi getApi() {
        return api;
    }

    private void setApi(ITweetApi api) {
        this.api = api;
    }
}
