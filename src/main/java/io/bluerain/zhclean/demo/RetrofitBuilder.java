package io.bluerain.zhclean.demo;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.logging.Logger;

public class RetrofitBuilder {

    private static Logger logger = Logger.getLogger(RetrofitBuilder.class.getName());

    private static Retrofit retrofit = null;

    public static Retrofit instance() {
        if (retrofit == null) {
            HttpLoggingInterceptor.Level level = HttpLoggingInterceptor.Level.BODY;
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(message -> {
                logger.info(message);
            });
            loggingInterceptor.setLevel(level);
            OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
            httpClientBuilder.addInterceptor(loggingInterceptor);
            retrofit = new Retrofit.Builder()

                    .baseUrl("https://blog-api.bluerain.io")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(httpClientBuilder.build())
                    .build();
        }
        return retrofit;
    }
}
