package io.bluerain.zhclean;

import io.bluerain.zhclean.core.StaticConfig;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class RetrofitBuilder {

    private static Logger logger = Logger.getLogger(RetrofitBuilder.class.getName());

    private static Retrofit retrofit = null;

    public static Retrofit instance() {
        if (retrofit == null) {
            retrofit = build(StaticConfig.AUTHORIZATION);
        }
        return retrofit;
    }

    private static Retrofit build(String authorization){
        // 拦截器：输出日志
        HttpLoggingInterceptor.Level level = HttpLoggingInterceptor.Level.BODY;
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(message -> logger.info(message));
        loggingInterceptor.setLevel(level);
        // 拦截器：添加 AUTHORIZATION header
        Interceptor addAuthorizationHeader = chain -> {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .header("accept", "application/json, text/plain, */*")
                    .header("authorization", authorization)
                    .header("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36")
                    .method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        };
        // 创建 okhttp client
        OkHttpClient httpClient =
                new OkHttpClient.Builder()
                        .addInterceptor(loggingInterceptor)
                        .addInterceptor(addAuthorizationHeader)
                        .writeTimeout(5, TimeUnit.SECONDS)
                        .build();
        return new Retrofit.Builder()
                .baseUrl("https://www.zhihu.com/api/v3/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient)
                .build();

    }
}
