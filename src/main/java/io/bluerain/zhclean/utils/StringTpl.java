package io.bluerain.zhclean.utils;


import org.apache.commons.text.StringSubstitutor;

import java.util.HashMap;
import java.util.Map;

public class StringTpl {

    public static String replace(String tplText, ValueMap valueMap) {
        return StringSubstitutor.replace(tplText, valueMap.toMap());
    }

    public static StringTplValueMapBuilder valueMap(String key, Object value) {
        Map<String, Object> valueMaps = new HashMap<>();
        valueMaps.put(key, value);
        return new StringTplValueMapBuilder(valueMaps);
    }

    public static class ValueMap {
        Map<String, Object> valueMaps;

        ValueMap(Map<String, Object> valueMaps) {
            this.valueMaps = valueMaps;
        }

        Map<String, Object> toMap() {
            return valueMaps;
        }
    }

    public static class StringTplValueMapBuilder {
        private Map<String, Object> valueMaps;

        StringTplValueMapBuilder(Map<String, Object> valueMaps) {
            this.valueMaps = valueMaps;
        }

        public StringTplValueMapBuilder valueMap(String key, Object value) {
            this.valueMaps.put(key, value);
            return this;
        }

        public ValueMap build() {
            return new ValueMap(this.valueMaps);
        }
    }
}
