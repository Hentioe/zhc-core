package io.bluerain.zhclean.services;

import io.bluerain.zhclean.RetrofitBuilder;
import io.bluerain.zhclean.models.Feed;
import io.bluerain.zhclean.sources.IFeedApi;
import io.reactivex.Observable;

public class FeedService {

    private IFeedApi api;

    public FeedService() {
        setApi(RetrofitBuilder.instance().create(IFeedApi.class));
    }

    public Observable<Feed> getList(int page) {
        if (page <= 0) page = 1;
        long afterId = (page - 1) * 7;
        if(afterId == 0)
            return getApi().getFeedFirst();
        return getApi().getFeed(afterId);
    }


    private IFeedApi getApi() {
        return api;
    }

    private void setApi(IFeedApi api) {
        this.api = api;
    }
}
