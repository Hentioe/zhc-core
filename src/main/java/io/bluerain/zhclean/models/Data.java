package io.bluerain.zhclean.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    /**
     * Feed 的 ID
     */
    @SerializedName("id")
    private String id;
    /**
     * Feed 目标
     */
    @SerializedName("target")
    private Target target;

    /**
     * 创建时间（Unix 时间戳）
     */
    @SerializedName("created_time")
    private long createdTime;

    /**
     * 创建时间（Unix 时间戳）
     */
    @SerializedName("updated_time")
    private long updatedTime;

    /**
     * 动态文字模板
     */
    @SerializedName("action_text_tpl")
    private String actionTextTpl;

    /**
     * 动作类型
     */
    @SerializedName("verb")
    private String verb;

    /**
     * 参与者列表
     */
    @SerializedName("actors")
    private List<Author> actors;

    /**
     * 简介（专栏简介等）
     */
    @SerializedName("intro")
    private String intro;

    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    public String getActionTextTpl() {
        return actionTextTpl;
    }

    public void setActionTextTpl(String actionTextTpl) {
        this.actionTextTpl = actionTextTpl;
    }

    public String getVerb() {
        return verb;
    }

    public void setVerb(String verb) {
        this.verb = verb;
    }

    public List<Author> getActors() {
        return actors;
    }

    public void setActors(List<Author> actors) {
        this.actors = actors;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public long getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(long updatedTime) {
        this.updatedTime = updatedTime;
    }
}
