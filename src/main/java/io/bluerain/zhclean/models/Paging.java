package io.bluerain.zhclean.models;

import com.google.gson.annotations.SerializedName;

public class Paging {

    /**
     * 是否为最后一页
     */
    @SerializedName("is_end")
    private boolean isEnd;
    /**
     * 下一页地址
     */
    @SerializedName("next")
    private String next;
    /**
     * 上一页地址
     */
    @SerializedName("previous")
    private String previous;

    public boolean isEnd() {
        return isEnd;
    }

    public void setEnd(boolean end) {
        isEnd = end;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }
}
