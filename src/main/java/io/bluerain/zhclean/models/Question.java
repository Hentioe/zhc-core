package io.bluerain.zhclean.models;

import com.google.gson.annotations.SerializedName;

public class Question {

    /**
     * ID
     */
    @SerializedName("id")
    private int id;
    /**
     * 创建时间戳
     */
    @SerializedName("created")
    private String created;
    /**
     * 链接地址
     */
    @SerializedName("url")
    private String url;
    /**
     * 标题
     */
    @SerializedName("title")
    private String title;
    /**
     * 摘要内容
     */
    @SerializedName("excerpt")
    private String excerpt;

    /**
     * 类型
     */
    @SerializedName("type")
    private String type;

    /**
     * 评论数量
     */
    @SerializedName("comment_count")
    private int commentCount;

    /**
     * 是否关注
     */
    @SerializedName("is_following")
    private String isFollowing;

    /**
     * 关注数量
     */
    @SerializedName("follower_count")
    private int followerCount;


    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public String getIsFollowing() {
        return isFollowing;
    }

    public void setIsFollowing(String isFollowing) {
        this.isFollowing = isFollowing;
    }

    public int getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(int followerCount) {
        this.followerCount = followerCount;
    }
}
