package io.bluerain.zhclean.models;

import com.google.gson.annotations.SerializedName;
import io.bluerain.zhclean.utils.StringTpl;

import java.io.Serializable;
import java.util.List;

public class Feed implements Serializable {

    /**
     * 分页信息
     */
    @SerializedName("paging")
    private Paging paging;
    /**
     * 数据是否结束
     */
    @SerializedName("is_end")
    private boolean isEnd;
    /**
     * 数据列表
     */
    @SerializedName("data")
    private List<Data> data;

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void setEnd(boolean end) {
        isEnd = end;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        String text = "填装完毕, 数据量:${size}\n${list}";
        StringBuilder listString = new StringBuilder();
        for (int i = 0; i < this.getData().size(); i++) {
            Data data = this.getData().get(i);
            String tpl = null;
            StringTpl.ValueMap valueMap = null;
            switch (data.getVerb()) {
                // 回答了问题
                case "TOPIC_ACKNOWLEDGED_ANSWER": // 来自话题的热门回答（非关注着）
                case "MEMBER_ANSWER_QUESTION":
                    tpl = "${author_name}回答了《${question_title}》, 内容为:\n ${content}\n";
                    valueMap = StringTpl
                            .valueMap("author_name", data.getTarget().getAuthor().getName())
                            .valueMap("question_title", data.getTarget().getQuestion().getTitle())
                            .valueMap("content", data.getTarget().getContent())
                            .build();
                    break;
                // 赞同了回答
                case "MEMBER_VOTEUP_ANSWER":
                    tpl = "${actor_name}赞同了[${author_name}]在" +
                            "《${question_title}》下的回答, 内容为:\n ${content}\n";
                    valueMap = StringTpl
                            .valueMap("actor_name", data.getActors().get(0).getName())
                            .valueMap("author_name", data.getTarget().getAuthor().getName())
                            .valueMap("question_title", data.getTarget().getQuestion().getTitle())
                            .valueMap("content", data.getTarget().getContent())
                            .build();
                    break;
                // 收藏了回答
                case "MEMBER_COLLECT_ANSWER":
                    tpl = "${actor_name}收藏了[${author_name}]在" +
                            "《${question_title}》下的回答, 内容为:\n ${content}\n";
                    valueMap = StringTpl
                            .valueMap("actor_name", data.getActors().get(0).getName())
                            .valueMap("author_name", data.getTarget().getAuthor().getName())
                            .valueMap("question_title", data.getTarget().getQuestion().getTitle())
                            .valueMap("content", data.getTarget().getContent())
                            .build();
                    break;
                // 关注了问题
                case "MEMBER_FOLLOW_QUESTION":
                    tpl = "${actor_name}关注了问题《${question_title}》\n";
                    valueMap = StringTpl
                            .valueMap("actor_name", data.getActors().get(0).getName())
                            .valueMap("question_title", data.getTarget().getTitle())
                            .build();
                    break;
                // 发表了文章
                case "MEMBER_CREATE_ARTICLE":
                    tpl = "${author_name}发表了文章《${article_title}》, 内容为:\n ${content}\n";
                    valueMap = StringTpl
                            .valueMap("author_name", data.getActors().get(0).getName())
                            .valueMap("article_title", data.getTarget().getTitle())
                            .valueMap("content", data.getTarget().getContent())
                            .build();
                    break;
                // 专栏热门文章
                case "COLUMN_POPULAR_ARTICLE":
                    tpl = "${author_name}发表了文章《${article_title}》, 内容为:\n ${content}\n";
                    valueMap = StringTpl
                            .valueMap("author_name", data.getTarget().getAuthor().getName())
                            .valueMap("article_title", data.getTarget().getTitle())
                            .valueMap("content", data.getTarget().getContent())
                            .build();
                    break;
                case "COLUMN_NEW_ARTICLE": // 专栏新增文章
                    tpl = "${author_name}发表了文章《${article_title}》, 内容为:\n ${content}\n";
                    valueMap = StringTpl
                            .valueMap("author_name", data.getActors().get(0).getName())
                            .valueMap("article_title", data.getTarget().getTitle())
                            .valueMap("content", data.getTarget().getContent())
                            .build();
                    break;
                // 点赞了文章
                case "MEMBER_VOTEUP_ARTICLE":
                    tpl = "${actor_name}点赞了文章《${article_title}》, 内容为:\n ${content}\n";
                    valueMap = StringTpl
                            .valueMap("actor_name", data.getActors().get(0).getName())
                            .valueMap("article_title", data.getTarget().getTitle())
                            .valueMap("content", data.getTarget().getContent())
                            .build();
                    break;
                // 关注了专栏
                case "MEMBER_FOLLOW_COLUMN":
                    tpl = "${actor_name}关注了专栏《${column_name}》, 简介: ${intro}\n";
                    valueMap = StringTpl
                            .valueMap("actor_name", data.getActors().get(0).getName())
                            .valueMap("column_name", data.getTarget().getTitle())
                            .valueMap("intro", data.getTarget().getContent())
                            .build();
                    break;
                // 关注了圆桌
                case "MEMBER_FOLLOW_ROUNDTABLE":
                    tpl = "${actor_name}关注了圆桌《${roundtable_name}》, 描述: ${description}\n";
                    valueMap = StringTpl
                            .valueMap("actor_name", data.getActors().get(0).getName())
                            .valueMap("roundtable_name", data.getTarget().getName())
                            .valueMap("description", data.getTarget().getDescription())
                            .build();
                    break;
                // 来自话题下的问题
                case "TOPIC_POPULAR_QUESTION":
                    tpl = "来自话题《${topic_name}》下的热门问题《${question_title}》, 内容为：\n${detail}\n";
                    valueMap = StringTpl
                            .valueMap("topic_name", data.getActors().get(0).getName())
                            .valueMap("title", data.getTarget().getTitle())
                            .valueMap("detail", data.getTarget().getDetail())
                            .build();
                    break;
                // 添加了问题
                case "MEMBER_ASK_QUESTION":
                    tpl = "${author_name}添加了问题：《${question_title}》, 内容为：\n${detail}\n";
                    valueMap = StringTpl
                            .valueMap("author_name", data.getActors().get(0).getName())
                            .valueMap("question_title", data.getTarget().getTitle())
                            .valueMap("detail", data.getTarget().getDetail())
                            .build();
                    break;
            }
            if (tpl == null || valueMap == null) {
                throw new RuntimeException("Unknown feed type: " + data.getVerb());
            }
            listString.append(StringTpl.replace(tpl, valueMap));
        }
        StringTpl.ValueMap valueMap = StringTpl
                .valueMap("size", this.getData().size())
                .valueMap("list", listString)
                .build();
        return StringTpl.replace(text, valueMap);
    }
}
