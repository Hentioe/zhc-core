package io.bluerain.zhclean.models;

import com.google.gson.annotations.SerializedName;

public class Target {

    /**
     * Feed 摘要内容
     */
    @SerializedName("excerpt")
    private String excerpt;
    /**
     * 目标问题（提问或回答需要）
     */
    @SerializedName("question")
    private Question question;

    /**
     * Feed ID
     */
    @SerializedName("id")
    private String id;

    /**
     * Feed 作者
     */
    @SerializedName("author")
    private Author author;

    /**
     * Feed 具体内容
     */
    @SerializedName("content")
    private String content;

    /**
     * Feed 类型
     */
    @SerializedName("type")
    private String type;
    /**
     * 标题（ 文章标题）
     */
    @SerializedName("title")
    private String title;


    /**
     * 名称（圆桌等）
     */
    @SerializedName("name")
    private String name;

    /**
     * 描述（圆桌等）
     */
    @SerializedName("description")
    private String description;


    /**
     * 详细内容（来自话题的热门问题等）
     */
    @SerializedName("detail")
    private String detail;

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}

