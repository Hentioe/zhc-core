package io.bluerain.zhclean.models;

import com.google.gson.annotations.SerializedName;

public class Author {

    /**
     * 是否关注
     */
    @SerializedName("is_followed")
    private boolean isFollowed;

    /**
     * 类型
     */
    @SerializedName("type")
    private String type;

    /**
     * 用户名称
     */
    @SerializedName("name")
    private String name;

    /**
     * 用户类型
     */
    @SerializedName("user_type")
    private String userType;

    /**
     * 头像地址
     */
    @SerializedName("avatar_url")
    private String avatarUrl;

    /**
     * 标题（专栏名称）
     */
    private String title;

    public boolean isFollowed() {
        return isFollowed;
    }

    public void setFollowed(boolean followed) {
        isFollowed = followed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
