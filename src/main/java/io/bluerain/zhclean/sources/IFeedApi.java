package io.bluerain.zhclean.sources;

import io.bluerain.zhclean.models.Feed;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IFeedApi {

    @GET("feed/topstory?action_feed=True&limit=7&action=down&desktop=true")
    Observable<Feed> getFeed(@Query("after_id") long afterId);

    @GET("feed/topstory?action_feed=True&limit=7&action=down&desktop=true")
    Observable<Feed> getFeedFirst();

}
