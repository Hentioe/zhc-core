package io.bluerain.zhclean;

public interface TestObserver<T> extends BaseTestObserver<T> {

    void onNext(T t);
}
