package io.bluerain.zhclean.demo;

import io.bluerain.zhclean.BaseServiceTest;
import io.bluerain.zhclean.TestObserver;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class TweetServiceTest extends BaseServiceTest<TweetService> {


    @Test
    public void getAllTest() {
        syncObsSubscribeOnNext(getService().getAll(), (TestObserver<List<TweetModel>>) list -> {
            assertNotNull(list);
            assertTrue(list.size() > 0);
        });
    }

}
