package io.bluerain.zhclean;


import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import static org.junit.Assert.assertNotNull;

public interface BaseTestObserver<T> extends Observer<T> {

    @Override
    default void onComplete() {

    }

    @Override
    default void onError(Throwable e) {
        e.printStackTrace();
        assert false;
    }

    @Override
    default void onNext(T t) {
        assertNotNull(t);
    }

    @Override
    default void onSubscribe(Disposable d) {

    }
}
