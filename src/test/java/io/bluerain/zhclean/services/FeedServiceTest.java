package io.bluerain.zhclean.services;

import io.bluerain.zhclean.BaseServiceTest;
import io.bluerain.zhclean.TestObserver;
import io.bluerain.zhclean.models.Data;
import io.bluerain.zhclean.models.Feed;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class FeedServiceTest extends BaseServiceTest<FeedService> {

    @Test
    public void getListTest()  {
        syncObsSubscribeOnNext(getService().getList(1), (TestObserver<Feed>) feed -> {
            assertNotNull(feed);
            assertNotNull(feed.getData());
            assertTrue(feed.getData().size() > 0);
            for (int i = 0; i < feed.getData().size(); i++) {
                Data data = feed.getData().get(i);
                assertNotNull(data.getActionTextTpl());
                assertNotNull(data.getActors());
                assertNotNull(data.getTarget());
                assertNotNull(data.getVerb());
            }
            // 输出内容
            try {
                logger.info(feed.toString());
            } catch (RuntimeException e) {
                assert false;
            }
        });
    }
}