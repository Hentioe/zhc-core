package io.bluerain.zhclean;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.schedulers.Schedulers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.logging.Logger;

public class BaseServiceTest<S> {

    protected Logger logger = Logger.getLogger(RetrofitBuilder.class.getName());

    private S service;

    @SuppressWarnings("unchecked")
    protected BaseServiceTest() {
        try {
            service = (S) ((Class)((ParameterizedType)this.getClass().
                    getGenericSuperclass()).getActualTypeArguments()[0]).getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    protected S getService() {
        return service;
    }

    protected <T> void
    syncObsSubscribeOnNext(Observable<T> observable, Observer<T> observer) {
        observable
                .observeOn(Schedulers.computation())
                .subscribe(observer);
    }
}
